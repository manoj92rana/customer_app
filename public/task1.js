function putdata() {    
    var url = "http://localhost:3000/task";
    var data = {};

    data.uname = document.getElementById('uname').value;
    //console.log(data.uname)
    data.email = document.getElementById('email').value;
    data.country = document.getElementById('country').value;

    console.log(JSON.stringify(data));
    var json = JSON.stringify(data);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url, true);
    xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    xhr.onload = function() {
        var custom = JSON.parse(xhr.responseText);
        if (xhr.readyState == 4 && xhr.status == "201") {
            console.table(custom);
            console.log(custom);
        } else {
            console.error(custom);
        }
    }
    xhr.send(json);

}

function onLoadPage() {
    var url = "http://localhost:3000/task";
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url, true)
    xhr.onload = function() {
        var custom = JSON.parse(xhr.responseText);
        console.log(custom);
        if (xhr.status == "200") {

            for (var item in custom) {
                var customdata = ({
                    '_id': custom[item]._id,
                    'uname': custom[item].uname,
                    'email': custom[item].email,
                    'country': custom[item].country

                });
                console.log(customdata);
                getdata(customdata);
                //           deletedata();
                console.log(customdata);
            }
        } else {
            console.error(custom);
        }
    }
    xhr.send(null);
}


function getdata(customdata) {

    var username = document.createElement('div');
    var email = document.createElement('div');

    var country = document.createElement('div');
    var editbtn1 = document.createElement('button');
    editbtn1.innerHTML = "EDIT";
    editbtn1.onclick = function() {
        location.href = "Edit_it.html?id="+customdata._id;

    }
    var delbtn1 = document.createElement('button');
    delbtn1.innerHTML = "DELETE";
    delbtn1.onclick = function() {
        //alert(customdata._id);
        deletedata(customdata._id);
    };
    var row1 = document.createElement('div');
       username.className = "div-table-col";
    email.className = "div-table-col";
    country.className = "div-table-col";
    row1.className = "div-table-row";
    delbtn1.className = "div-table-col";
    editbtn1.className = "div-table-col";

    row1.id = customdata._id;
    row1.appendChild(username);
    row1.appendChild(email);
    row1.appendChild(country);
    row1.appendChild(editbtn1);
    row1.appendChild(delbtn1);

    username.innerHTML = customdata.uname;
    email.innerHTML = customdata.email;
    country.innerHTML = customdata.country;

    var container1 = document.getElementById('container');
    container1.appendChild(row1);
}

function deletedata(id) {
    var xhr = new XMLHttpRequest();
    xhr.withCredentials = true;

    xhr.addEventListener("readystatechange", function() {
        if (this.readyState === 4) {
            console.log(this.responseText);
        }
    });

    xhr.open("DELETE", "http://localhost:3000/task/"+id);
    
    xhr.send(null);
    var divtable = document.getElementById('container');
    var remove = document.getElementById(id);
    console.log(remove);
    divtable.removeChild(remove);
    
}