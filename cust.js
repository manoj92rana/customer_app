var express = require('express')
var app = express()
var bodyparser = require('body-parser');  
var MongoClient = require('mongodb').MongoClient;
var dburl = "mongodb://localhost:27017/customs";

app.use(bodyparser.json());

//static path 
app.use(express.static(__dirname + '/public'));
//post
app.post('/task', function(req, res) {
    console.log(req.body);
    MongoClient.connect(dburl, (err, db) => {
        if (err) {
            return console.log('Not connected');
        }
        var cus = { uname: req.body.uname, email: req.body.email, country: req.body.country };
        db.collection('custom').insert(cus, (err, result) => {
            //    console.log(result);
            if (err) {
                return res.status(400).send("error");
            }
            return res.status(200).send(result.pos);
        });
        db.close();
    });
});
  //get
  app.get('/task', function(req, res, next) {
      MongoClient.connect(dburl, function(err, db) {
          if (err) { console.log(err); throw err; }
          db.collection('custom').find().toArray(function(err, docs) {
              if (err) throw err;
              res.send(docs);
              db.close();
          });
      });
  });

  //get id 
  app.get('/task/:id', (req, res) => {

  var ObjectID = require('mongodb').ObjectID;
      var id = req.params.id;
      var objid = new ObjectID(id);
      console.log(id + ":" + objid);


      MongoClient.connect(dburl, (err, db) => {
          if (err) {
              return console.log('Not connected');
          }
          console.log('Connected');
          db.collection('custom').find({ _id: objid }).toArray(function(err,docs) {
               if (err) throw err;
              res.send(docs)
           db.close();
          })  ;
          
      });

  });
//delete
app.delete('/task/:id',(req, res) => {
var ObjectID = require('mongodb').ObjectID;
    var id = req.params.id;
      console.log(id);
    var objid = new ObjectID(id);
    MongoClient.connect(dburl, (err, db) => {
        if (err) {
            return err;
        }
        db.collection('custom').remove({_id:objid }, (err, result) => {
            if (err) {
                return res.status(400).send("error");
            }
            return res.status(200).send(result.ops);
            console.log('successfully deleted');
        });
        db.close();
    });
});



//updated
app.put('/task/:id', (req, res) => {
    var ObjectID = require('mongodb').ObjectID;
    var id = req.params.id;
    var objid = new ObjectID(id);
    console.log(objid);
   var uname =req.body.uname;
   var email =req.body.email;
   var country = req.body.country;

    console.log(id);
    MongoClient.connect(dburl, (err, db) => {
        if (err) {
            return console.log('Not connected');
        }
        console.log('Connected');
        db.collection('custom').update({ _id: objid }, { $set: { "uname":uname ,"email": email ,"country" : country } }, (err, result) => {
            if (err) {
                return res.status(400).send("error");
            }
            return res.send("Updated .."),res.status(200).send(result.ops);
                  });
        db.close();
    });
});

app.listen(3000, () => console.log('Example app listening on port 3000!'))
